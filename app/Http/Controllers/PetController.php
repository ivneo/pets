<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pet;
use DB;
class PetController extends Controller
{
    //

    public function index()
    {
        //$pets = Pet::all();
        $pets = DB::table('pets')->get();

        return view('pets.index', compact('pets'));
    }


    public function store(Request $request)
    { 
      $request->validate([
        'nombre'=>'required|max:50',
        'edad'=> 'required|integer|max:3',
        'especie' => 'required|max:30',
        'clasificacion' => 'max:20',
        'peso' => 'required|integer|max:100',
        'pais_origen' => 'required|max:30'
      ],[
        'nombre.required'=>'El nombre es requerido',
        'nombre.max'=>'El nombre no debe tener mas de 50 caracteres',
        'edad.required'=>'la edad es requerida',
        'edad.max'=>'La edad es expresada en año, no debe tener más de 3 dígitos',
        'edad.integer' => 'La edad debe ser un número',
        'especie.required'=>'La especie es requerida',
        'especie.max'=>'La especie no debe tener más de 30 dígitos',
        'clasificacion.max'=>'La clasificacionno debe tener más de 20 dígitos',
        'peso.required'=>'El peso es requerido',
        'peso.max'=>'El peso no debe tener mas de 3 caracteres',
        'peso.integer' => 'El peso debe ser un número',
        'pais_origen.required'=>'El pais de origen es requerido',
        'pais_origen.max'=>'El pais de origen no debe tener mas de 30 caracteres',
      ]);
      


/*
      $pet = new Pet([
        'nombre' => $request->get('nombre'),
        'edad'=> $request->get('edad'),
        'especie'=> $request->get('especie'),
        'clasificacion'=> $request->get('result'),
        'peso'=> $request->get('peso'),
        'pais_origen'=> $request->get('pais_origen'),
      ]);
      $pet->save();
*/

      DB::table('pets')->insert(
     [  'nombre' => $request->get('nombre'),
        'edad'=> $request->get('edad'),
        'especie'=> $request->get('especie'),
        'clasificacion'=> $request->get('result'),
        'peso'=> $request->get('peso'),
        'pais_origen'=> $request->get('pais_origen'), ]
      );


      return redirect('/pets')->with('success', 'La mascota se ha agregado correctamente');
    }


     public function edit($id)
    {
        //$pet = Pet::find($id);
       $pet = DB::table('pets')->where('id',$id)->first();

        return view('pets.edit', compact('pet'));
    }


    public function update(Request $request, $id)
    {
      $request->validate([
        'nombre'=>'required|max:50',
        'edad'=> 'required|integer|max:100',
        'especie' => 'required|max:30',
        'clasificacion' => 'max:20',
        'peso' => 'required|integer|max:100',
        'pais_origen' => 'required|max:30'
      ],[
        'nombre.required'=>'El nombre es requerido',
        'nombre.max'=>'El nombre no debe tener mas de 50 caracteres',
        'edad.required'=>'la edad es requerida',
        'edad.max'=>'La edad es expresada en año, no debe tener más de 3 dígitos',
        'edad.integer' => 'La edad debe ser un número',
        'especie.required'=>'La especie es requerida',
        'especie.max'=>'La especie no debe tener más de 30 dígitos',
        'clasificacion.max'=>'La clasificacionno debe tener más de 20 dígitos',
        'peso.required'=>'El peso es requerido',
        'peso.max'=>'El peso no debe tener mas de 3 caracteres',
        'peso.integer' => 'El peso debe ser un número',
        'pais_origen.required'=>'El pais_origen es requerido',
        'pais_origen.max'=>'El pais_origen no debe tener mas de 30 caracteres',
      ]);
      
      
      DB::table('pets')->where('id',$id)->update(
     [  'nombre' => $request->get('nombre'),
        'edad'=> $request->get('edad'),
        'especie'=> $request->get('especie'),
        'clasificacion'=> $request->get('result'),
        'peso'=> $request->get('peso'),
        'pais_origen'=> $request->get('pais_origen'), ]
      );

      
      /*
      $pet = new Pet([
        'nombre' => $request->get('nombre'),
        'edad'=> $request->get('edad'),
        'especie'=> $request->get('especie'),
        'clasificacion'=> $request->get('result'),
        'peso'=> $request->get('peso'),
        'pais_origen'=> $request->get('pais_origen'),
      ]);
      $pet->save();
*/




      return redirect('/pets')->with('success', 'La mascota se ha editado correctamente');
    }


public function destroy($id)
{
/*
     $pet = Pet::find($id);
     $pet->delete();

*/
     
      DB::table('pets')->where('id',$id)->delete();
     
     return redirect('/pets')->with('success', 'Stock has been deleted Successfully');
}
public function create(){
    return view('pets.create');
}

}



