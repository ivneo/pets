<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pet extends Model
{
    protected $fillable = [
    'nombre',
    'edad',
    'especie',
    'clasificacion',
    'peso',
    'pais_origen',
  ];
}
