@extends('layout')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Edit Share
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('pets.update', $pet->id) }}">
        @method('PATCH')
        @csrf
        <div class="form-group">
              @csrf
              <label for="nombre">Nombre:</label>
              <input type="text" class="form-control" name="nombre" value="{{ $pet->nombre}}" />
          </div>
          <div class="form-group">
              <label for="edad">Edad:</label>
              <input type="text" class="form-control" name="edad"  value="{{ $pet->edad}}"/>
          </div>
          <div class="form-group">
              <label for="especie">Especie:</label>
              <input type="text" class="form-control" name="especie"  value="{{ $pet->especie}}"/>
          </div>
          
          <div class="form-group">
              <label for="clasificacion">Clasificacion</label>
              <br>
              @if($pet->clasificacion == 'salvaje')
            <input type="radio" name="result" value="Salvaje" checked > Salvaje
            <input type="radio" name="result" value="Domestico"> Domestico
            @else
            <input type="radio" name="result" value="Salvaje"  > Salvaje
            <input type="radio" name="result" value="Domestico" checked> Domestico
            @endif
            
          <br><br>

      
          <div class="form-group">
              <label for="peso">Peso:</label>
              <input type="text" class="form-control" name="peso"  value="{{ $pet->peso}}"/>
          </div>

          <div class="form-group">
              <label for="pais_origen">Pais origen:</label>
              <input type="text" class="form-control" name="pais_origen"  value="{{ $pet->pais_origen}}"/>
          </div>
          <button type="submit" class="btn btn-primary">Agregar</button>
      </form>
  </div>
</div>
@endsection