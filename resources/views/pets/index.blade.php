@extends('layout')

@section('content')
<style>
  .uper {
    margin-top: 39px;
  }
  #b{
    margin-right: -10px;
  }
  #d{
    margin-right: -20px;
  }
</style>
<div class="uper">
  @if(session()->get('success'))
    <div id="c"class="alert alert-success">
       <button id="cerrar" type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {{ session()->get('success') }}  
    </div><br />

    <script>
      var button = document.querySelector('#cerrar');
     button.addEventListener('click', function(){
        document.querySelector('#c').style.display = 'none';
     });
    </script>




  @endif
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Nombre</td>
          <td>Edad</td>
          <td>Especie</td>
          <td>Clasificación</td>
          <td>Peso</td>
          <td>Pais de origen</td>
          <td colspan="2">Acción</td>
        </tr>
    </thead>
    <tbody>
        @foreach($pets as $pet)
        <tr>
            <td>{{$pet->id}}</td>
            <td>{{$pet->nombre}}</td>
            <td>{{$pet->edad}}</td>
            <td>{{$pet->especie}}</td>
            <td>{{$pet->clasificacion}}</td>
            <td>{{$pet->peso}}</td>
            <td>{{$pet->pais_origen}}</td>
            <td><a id="b" href="{{ route('pets.edit',$pet->id)}}" class="btn btn-primary">Edit</a></td>
            <td>
                <form action="{{ route('pets.destroy', $pet->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button  id="d"class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
            
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
  
@endsection