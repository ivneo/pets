@extends('layout')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Añadir mascota
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('pets.store') }}">
          <div class="form-group">
              @csrf
              <label for="nombre">Nombre:</label>
              <input type="text" class="form-control" name="nombre" value="{{old('nombre')}}" />
          </div>
          <div class="form-group">
              <label for="edad">Edad:</label>
              <input type="text" class="form-control" name="edad" value="{{old('edad')}}" />
          </div>
          <div class="form-group">
              <label for="especie">Especie:</label>
              <input type="text" class="form-control" name="especie" value="{{old('especie')}}" />
          </div>
          
          <div class="form-group">
              <label for="clasificacion">Clasificacion</label>
              <br>
            <input type="radio" name="result" value="Salvaje" checked> Salvaje
            <input type="radio" name="result" value="Domestico"> Domestico
          <br><br>

      
          <div class="form-group">
              <label for="peso">Peso:</label>
              <input type="text" class="form-control" name="peso" value="{{old('peso')}}" />
          </div>

          <div class="form-group">
              <label for="pais_origen">Pais origen:</label>
              <input type="text" class="form-control" name="pais_origen" value="{{old('pais_origen')}}" />
          </div>
          <button type="submit" class="btn btn-primary">Agregar</button>
      </form>
  </div>
</div>
@endsection